<?php

/**
 * @file
 * Salesforce Entityform inc file to manage options form for entityform.
 */

/**
 * Callback function salesforce_entityform_form_manage().
 *
 * @return array
 *   Returns list of entityform in a table structure.
 */
function salesforce_entityform_form_manage() {

  $data = salesforce_entityform_sf_entityform_options(
            array('type', 'label', 'data'),
            array(),
            'select',
            '{entityform_type}'
          );
  if (empty($data)) {
    // Check if data exists.
    return t("No Entityform Exists!");
  }
  // Table header array.
  $header = array('ID', 'Title', 'Type', 'Action');
  $rows = array();
  // Create Item array.
  foreach ($data as $key => $val) {
    $rows[] = array(++$key,
            array(
              'data' => l(
                $val->label,
                drupal_get_path_alias("eform/submit/" . str_replace("_", "-", $val->type))
              ),
              'align' => 'left',
            ),
      'Entityform',
      l(t("Edit"),
        "admin/config/services/salesforce_entityform/manage/" . $val->type . "/edit"),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Callback function salesforce_entityform_form_manage_edit().
 */
function salesforce_entityform_form_manage_edit($form, &$form_state) {

  $entity_id = (arg(1) != 'ajax') ? $form_state['storage']['entity_id'] = arg(5) : $form_state['storage']['entity_id'];

  $field_instance = field_info_instances('entityform', $entity_id);

  // Check if invalid entity id return.
  if (empty($field_instance)) {
    drupal_set_message(check_plain("Entityform not exists for entity id " . $entity_id), 'error');
    return $form;
  }

  $select = salesforce_entityform_sf_entityform_options(
      array('entityform_id',
        'sf_key',
        'sf_ef_key',
        'generate_entity',
        'generate_entity_type',
      ),
      array(
        'entityform_id' => array($entity_id, '='),
        'status' => array(1, '='),
      ),
      'select');

  $form_state['storage']['salesforce'] = isset($form_state['storage']['salesforce']) ? $form_state['storage']['salesforce'] : count($select);

  $form['entity_id'] = array(
    '#type' => 'hidden',
    '#value' => $entity_id,
  );

  $form['salesforce_entityform'] = array(
    '#type' => 'fieldset',
    '#title' => t("Manage Fields"),
    "#tree" => TRUE,
    '#prefix' => '<div id="salesforce_entityform">',
    '#suffix' => '</div>',
  );

  for ($i = 0; $i <= $form_state['storage']['salesforce']; $i++) {

    $options = salesforce_entityform_create_options($entity_id);
    $title = isset($select[$i]->sf_key) ? 'Field ' . ($i + 1) : 'Empty';

    $form['salesforce_entityform'][$i] = array(
      '#type' => 'fieldset',
      '#title' => $title,
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['salesforce_entityform'][$i]['salesforce_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Salesforce Id'),
      '#prefix' => '<div style="display:inline; width:48%; float:left;">',
      '#suffix' => '</div>',
      '#default_value' => isset($select[$i]->sf_key) ? $select[$i]->sf_key : '',
    );
    $form['salesforce_entityform'][$i]['entityform_id'] = array(
      '#type' => 'select',
      '#title' => t('Entity Form Id'),
      '#prefix' => '<div style="display:inline;  width:48%; float:right;">',
      '#suffix' => '</div>',
      '#options' => $options,
      '#default_value' => isset($select[$i]->sf_ef_key) ? $select[$i]->sf_ef_key : '',
    );
  }

  $form['salesforce_entityform']['add_more'] = array(
    "#type" => 'button',
    '#value' => 'Add More',
    '#href' => '',
    '#ajax' => array(
      'callback' => 'salesforce_entityform_add_option',
      'wrapper' => 'salesforce_entityform',
    ),
  );
  $form['generate_entity'] = array(
    "#type" => 'checkbox',
    '#default_value' => isset($select[0]->generate_entity) ? $select[0]->generate_entity : 1,
    '#title' => 'Generate Salesforce Entity',
  );

  $entity_type_options = array(
    SALESFORCE_ENTITYFORM_TYPE_WEB_TO_LEAD => t('Lead'),
    SALESFORCE_ENTITYFORM_TYPE_WEB_TO_CASE => t('Case'),
  );
  $form['generate_entity_type'] = array(
    '#type' => 'radios',
    '#title' => 'Select entity type',
    '#options' => $entity_type_options,
    '#states' => array(
      'invisible' => array(
        ':input[name="generate_entity"]' => array('checked' => FALSE),
      ),
    ),
    '#default_value' => isset($select[0]->generate_entity_type) ? $select[0]->generate_entity_type : SALESFORCE_ENTITYFORM_TYPE_WEB_TO_LEAD,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => 'Save');
  $form_state['storage']['salesforce']++;

  return $form;
}

/**
 * Validation callback function on Edit form.
 */
function salesforce_entityform_form_manage_edit_validate($form, &$form_state) {
  $values = $form_state['values']['salesforce_entityform'];
  $duplicate = array('salesforce' => array());
  foreach ($values as $key => $value) {
    // Check if salesforce id is empty.
    if (is_array($value) && (empty($value['salesforce_id']) && $value['entityform_id'] != '_none')) {
      form_set_error("salesforce_entityform][$key][salesforce_id", t('Please Enter a value!'));
    }
    // Check for empty entityform id.
    elseif (!empty($value['salesforce_id']) && $value['entityform_id'] == '_none') {
      form_set_error("salesforce_entityform][$key][entityform_id", t('Please select a valid value!'));
    }
    // Check for duplicate salesforce id.
    if (is_array($value) && !empty($value['salesforce_id']) && in_array($value['salesforce_id'], $duplicate['salesforce'])) {
      form_set_error("salesforce_entityform][$key][salesforce_id", t('Duplicate value for Salesforce Id field!'));
    }

    $duplicate['salesforce'][] = is_array($value) ? $value['salesforce_id'] : '';
  }
}

/**
 * Callaback function to add option.
 *
 * @return array
 *   Return the form element.
 */
function salesforce_entityform_add_option($form, $form_state) {
  return $form['salesforce_entityform'];
}

/**
 * Submit handler for edit form.
 */
function salesforce_entityform_form_manage_edit_submit($form, &$form_state) {
  // Set the entity form id.
  $entity_id = $form_state['values']['entity_id'];

  // Check if no entity id return.
  if (!$entity_id) {
    return drupal_set_message(t("Invalid entity id"));
  }
  $values = $form_state['values']['salesforce_entityform'];
  $val['generate_entity'] = $form_state['values']['generate_entity'];
  $val['generate_entity_type'] = $form_state['values']['generate_entity_type'];

  salesforce_entityform_sf_entityform_options(array('status' => 0),
        array(
          'entityform_id' => array($entity_id, '='),
          'status' => array(1, '='),
        ),
        'update');
  foreach ($values as $data) {
    if (!is_array($data)) {
      continue;
    }
    // Set the condition and value to be stored in db.
    $cond = array();
    $val['sf_key'] = $data['salesforce_id'];
    $val['sf_ef_key'] = $data['entityform_id'];

    if (empty($val['sf_key'])) {
      $cond['sf_key'] = array($data['salesforce_id'], '=');
    }

    if (empty($val['sf_key'])) {
      $cond['sf_ef_key'] = array($data['entityform_id'], '=');
    }
    $val['entityform_id'] = $entity_id;

    salesforce_entityform_sf_entityform_options($val, $cond, 'insert');
  }

}

/**
 * Callback function for database query.
 *
 * @param array $val
 *   Array of values.
 * @param array $cond
 *   Array of conditions.
 * @param string $type
 *   Type of query i.e. Select, Upadate, Insert or Delete.
 * @param string $table
 *   Table name.
 *
 * @return array
 *   Return a set of array value in some cases.
 */
function salesforce_entityform_sf_entityform_options(array $val = array(), array $cond = array(), $type = '', $table = '{sf_entityform_options}') {
  if ($type == '') {
    return NULL;
  }
  $error_string = '';

  switch ($type) {
    case 'select':
      try {
        $select = db_select($table, 't')->fields('t', $val);
        if (!empty($cond)) {
          foreach ($cond as $key => $value) {
            $select->condition($key, $value[0], $value[1]);
          }
        }
        return $select->execute()->fetchAll();
      }
      catch (Exception $e) {
        $error_string = "Error in select query: " . $e->getMessage();
        watchdog('sf_ef_select', $error_string, array(), WATCHDOG_ERROR, NULL);
      }
      break;

    case 'update':
      $val['updated'] = $_SERVER['REQUEST_TIME'];
      try {
        $update = db_update($table)->fields($val);
        if (!empty($cond)) {
          foreach ($cond as $key => $value) {
            $update->condition($key, $value[0], $value[1]);
          }
        }
        return $update->execute();
      }
      catch (Exception $e) {
        $error_string = "Error in update query: " . $e->getMessage();
        watchdog('sf_ef_update', $error_string, array(), WATCHDOG_ERROR, NULL);
      }
      break;

    case 'insert':
      if ($val['sf_ef_key'] != '_none') {
        try {
          $val['status'] = 1;
          $val['updated'] = $_SERVER['REQUEST_TIME'];
          $val['created'] = $_SERVER['REQUEST_TIME'];
          db_insert($table)->fields($val)->execute();
        }
        catch (Exception $e) {
          $error_string = "Error in insert query: " . $e->getMessage();
          watchdog('sf_ef_insert', $error_string, array(), WATCHDOG_ERROR, NULL);
        }
      }
      break;

    case 'delete':
      try {
        $delete = db_delete($table);
        if (!empty($cond)) {
          foreach ($cond as $key => $value) {
            $delete->condition($key, $value[0], $value[1]);
          }
        }
        $delete->execute();
      }
      catch (Exception $e) {
        $error_string = "Error in delete query: " . $e->getMessage();
        watchdog('sf_ef_delete', $error_string, array(), WATCHDOG_ERROR, NULL);
      }
      break;

    default:
      break;
  }
}

/**
 * Callback function to set option value.
 *
 * @param string $entity_id
 *   Entity id for the current entityform.
 *
 * @return array
 *   Return an array of option value.
 */
function salesforce_entityform_create_options($entity_id = '') {
  // Check for entity id.
  $entity_id = ($entity_id == '') ? arg(5) : $entity_id;

  // Get all the instance of entityform field.
  $field_instance = field_info_instances('entityform', $entity_id);

  $option["_none"] = "--Select--";

  if (empty($field_instance)) {
    return $option;
  }

  foreach ($field_instance as $key => $instance) {
    $label = $instance['label'];
    $field = field_info_field($key);
    if (!empty($field)) {
      if (($field['module'] == 'list') && $field['cardinality'] != 1) {
        foreach ($field['settings']['allowed_values'] as $key => $val) {
          // Set option value for Select, checkbox etc.
          $option[$field['field_name'] . "--" . $key] = $label . "__" . $val;
        }
      }
      else {
        // Set option value in input field.
        $option[$field['field_name']] = $label;
      }
    }
  }
  return $option;
}
