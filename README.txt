Introduction
------------
This module allows generating leads and cases to Salesforce using drupal
entityform.

Installation
-------------
Once you activate the module it sets up an entity administration interface under
Administration > Configuration > Web Services > Salesforce Settings

Usage
---------------
1. Enable the module
2. Goto admin/config/services/salesforce_entityform
3. Click "Settings"
4. Enter Salesforce OID or ORGID depending on what entity you want to create
   (case or lead) and Salesforce Lead URL or Case URL.
   The OID and ORGID value is your instance of Salesforce.com.
   That doesn’t show up in very many places, but if you navigate to
   Setup > Administration Setup > Company Profile > Company Information,
   you’ll see your OID listed as a field on that page as well.
5. Click "Save Configuration"
6. Click Manage Tab. In this tab all the entityform created are listid.
7. Click Edit link in order to generate lead from the form.
8. Map entitform field with the salesform feilds.
7. Check Generate Salesforce Entity checkbox if you want to start generating
   entities from the entityform. Select what type of entity you want to create.
8. Click "Save" to save the form.
9. Navigate to entityform and submit that form to generate salesforce entity.

Module Integration
---------------------
The aim of this module is generate salesforce lead and case from entityform
using the salesforce OID / ORGID.
